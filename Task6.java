package ua.org.oa.scorpikity.practice1;

/**
 * Created by Kity on 23.06.2016.
 */
public class Task6 {
    private int a = 0;
    private int b = 0;

    public Task6(int a, int b){
        this.a =a;
        this.b =b;
    }
        public void methodTask6(){
            int sum = 0;
            for(int i =a; i<=b; i++){
                if(i%2==0)
                    sum+=i;
            }
            System.out.println("Sum of even numbers from " + a + " to " + b + " is " + sum);
        }


}
