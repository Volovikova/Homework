package ua.org.oa.scorpikity.practice1;

/**
 * Created by Kity on 14.07.2016.
 */
public interface SympleList {
    public void add(String s);
    public String get();
    public String get(int id);
    public String remove();
    public String remove(int id);
    public boolean delete();

}
