package ua.org.oa.scorpikity.practice1;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Kity on 18.08.2016.
 */
public class Weather {
    private String month = null;
    private int dayQuantity = 0;
    private boolean correctMonth = false;
    private double averageTemperature =0;
        public void averageTemperature(){
            Scanner scanner = new Scanner(System.in);
            do{System.out.println("Введите месяц ");
                month = scanner.nextLine();
                switch (month){
                    case "Январь":
                case "Март":
                case "Май":
                    case "Июль":
                    case "Август":
                    case "Октябрь":
                    case "Декабрь":
                    dayQuantity = 30;
                        correctMonth = true;
                        break;
                    case "Апрель":
                    case "Июнь":
                    case "Сентябрь":
                        case "Ноябрь":
                            dayQuantity = 31;
                            correctMonth = true;
                            break;
                    default:
                        System.out.println("Название месяца введено не корректно, повторите ввод: ");
                        break;}
            }
            while (!correctMonth);
            ArrayList<Double>temperature = new ArrayList<>();
            double temp = 0;
            double maxTemp = -50;
            double minTemp = 50;
            int maxTempDay = 0;
            int minTempDay = 0;
            double sumTemp = 0;
            for(int i = 0; i<dayQuantity;){
                i++;
                System.out.println("Введите температуру для " + i + "-го числа");
                temp = scanner.nextDouble();
                if(-50<temp&&temp<50){
                    temperature.add(temp);
                    sumTemp += temp;
                    if(maxTemp<temp){
                        maxTemp = temp;
                        maxTempDay = i;
                    }
                    if(minTemp>temp){
                        minTemp = temp;
                        minTempDay = i;
                    }

                }
                else {
                    System.out.println("Введено некорректное значение температуры, повторите ввод: ");
                    i--;
                }
            }
            scanner.close();
            averageTemperature = sumTemp/dayQuantity;
            System.out.println("Среднемесячная температура: " + averageTemperature);
            System.out.println("Максимальная температура была " + maxTempDay + "-го и составила " + maxTemp + "градусов");
            System.out.println("Минимальная температура была " + minTempDay + "-го и составила " +maxTemp + "градусов");

    }

}

