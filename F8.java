package ua.org.oa.scorpikity.practice1;

/**
 * Created by Kity on 17.07.2016.
 */
public class F8 {
    public  static  void  main(String[] args){
        String s1 = "Language";
        String sub1 = s1.substring(1);
        String s2 = "Result";
        String sub2 = s2.substring(1);
        String s3 = sub1 + sub2;
        System.out.println(s3);
    }
}
