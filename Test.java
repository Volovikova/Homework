package ua.org.oa.scorpikity.practice1;

/**
 * Created by Kity on 14.07.2016.
 */
public class Test {
    public static void main(String [] args){
        DynamicStringList list= new DynamicStringList();
        list.add("0");
        list.add("1");
        System.out.println(list.get());
        System.out.println(list.get(0));
        list.add("2");
        list.add("3");
        System.out.println(list);
        System.out.println(list.remove(2));
        System.out.println(list.remove());
        System.out.println(list.remove());
        System.out.println(list.remove());
    }
}
