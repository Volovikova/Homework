package ua.org.oa.scorpikity.practice1;



/**
 * Created by Kity on 26.06.2016.
 */
public class StringHelperTest {
    public static void main(String[] args){
        System.out.println("~~~Test for first task~~~");
        String s1 = "HHello";
        String s2 = "WWorld";
        String result = StringHelper.customConcat(s1,s2);
        System.out.println("First string is: " + s1);
        System.out.println("Second string is: " + s2);
        System.out.println("Method result is: "+ result);


    }

}
